from __future__ import print_function
import timeit

import schedule
import time
import os
import sys
import traceback
import logging
import argparse

# import squidlib modules
sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))
from sqapi.datasources2 import SQAPIDatasources, api_argparser

# setup logfile
logfile = "{}.log".format(os.path.realpath(__file__))
logging.basicConfig(filename=logfile, level=logging.INFO, format='%(asctime)s %(message)s',
                    datefmt='%m/%d/%Y %I:%M:%S %p')

# parse arguments
api_argparser.add_argument("--platform", type=str, help="The platform name to use")
api_argparser.add_argument("--verbosity", help="Set the level of verbosity (0,1,2)", action="count", default=1)
#api_argparser.add_argument("--runonce", help="No schedule, just run once", action='store_true', default=True)
api_argparser.add_argument("--every", type=str, help="Day of the week to repeat import, eg: 'sunday'", default="sunday")
api_argparser.add_argument("--at", type=str, help="Time of day to run parser, eg: '14:00'", default=None)


def sync_deployments(sqapi, platform):
    logging.info("*** STARTING SYNCHRONISATION OF DATA FOR PLATFORM: {}".format(platform["name"]))
    campaign_list = sqapi.get_datasource_list(platform['data'])
    user = sqapi.get_user(token=sqapi.api_token)
    stats = {"deployment_count":0, "deployments_saved":0, "errors":0}
    start_time = timeit.default_timer()
    for c in campaign_list:
        campaign_name = os.path.basename(os.path.normpath(c))
        deployment_list = sqapi.get_datasource_list(platform['data'], urlkey=c)
        for d in deployment_list:
            deployment_name = os.path.basename(os.path.normpath(d))
            try:
                status = sqapi.import_deployment(platform, user, campaign_name, deployment_name)
                stats["deployment_count"] += 1
                stats["deployments_saved"] += 1 if status > 0 else 0
                stats["errors"] += 1 if status < 0 else 0
            except KeyboardInterrupt:
                schedule.CancelJob()
                sys.exit()
            except Exception as e:
                traceback.print_exc()
                print("DEPLOYMENT IMPORT ERROR: {}".format(d))
                logging.error("DEPLOYMENT IMPORT ERROR: {} ({})".format(d, e))
                logging.error(e, exc_info=True)

    logging.info("*** ENDING SYNCHRONISATION FOR PLATFORM: {}... Took {}s (stats: {})".format(platform["name"], timeit.default_timer()-start_time, stats))


if __name__ == "__main__":

    args = api_argparser.parse_args()

    # setup api
    sqapi = SQAPIDatasources(api_token=args.api_key, url=args.url)
    platform = sqapi.get_platform_ui()

    if args.at is None:
        print("Running once...")
        sync_deployments(sqapi, platform)
    else:
        # set to run every sunday at midnight
        print ("Starting scheduler: will run every %s at %s..." % (args.every.lower(), args.at))
        getattr(schedule.every(), args.every.lower()).at(args.at).do(sync_deployments)
        #schedule.every(20).seconds.do(sync_deployments)

        while True:
            schedule.run_pending()
            time.sleep(30)   # poll every 30 second increments


