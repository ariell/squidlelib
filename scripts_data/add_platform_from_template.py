import sys, os
from glob import glob
import json
from pprint import pprint

sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))
from sqapi.datasources2 import SQAPIDatasources, api_argparser
from sqapi.ui import UIComponents

thisdir = os.path.realpath(os.path.dirname(__file__))

platformfile_pattern = os.path.join(thisdir,"..","platform_templates","*.json")

print(platformfile_pattern)

api_argparser.add_argument('--platformfile', default=None, type=str, help='path to json file with platform info')


if __name__ == "__main__":
    args = api_argparser.parse_args()
    sqapi = SQAPIDatasources(api_token=args.api_key, url=args.url)
    user = sqapi.get_user(token=sqapi.api_token)

    platformfile = args.platformfile
    if platformfile is None:
        platform_templates = [dict(name=os.path.basename(f), path=f) for f in glob(platformfile_pattern)]
        _, ind = UIComponents.select_list("Select a platform", [o.get("name") for o in platform_templates])
        platformfile = platform_templates[ind].get("path")

    with open(platformfile) as f:
        data = json.load(f)
        #pprint(data)

    platform = sqapi.new_platform(data.get("name"), data.get("description"), user.get("id"), data.get("data"))
    pprint(platform)
    # print(platformfile)