import os
import random
import sys

sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))

from sqapi.classifiers import SQAPIBaseClassifier, api_argparser

# Add additional arguments as you need for your classifier
api_argparser.add_argument("--media_cache_dir", type=str, help="Path to cache image downloads", required=False, default=None)


class RandomClassifier(SQAPIBaseClassifier):
    def __init__(self, media_cache_dir, **kwargs):
        SQAPIBaseClassifier.__init__(self, **kwargs)
        self.media_cache_dir = media_cache_dir
        self.labels = ["MALCB", "BIOTA", "P"]

    def predict_point(self, p, mediaobj, **kwargs):
        """
        Overridden method: predict label for x-y point
        """
        # image_data = mediaobj.data()            # cv2 image object containing media data
        # media_path = mediaobj.url               # path to media item
        # x = int(p.get("x")*mediaobj.width)      # label x coord in pixels
        # y = int(p.get("y")*mediaobj.height)     # label y coord in pixels

        classifier_code = random.sample(self.labels, 1)[0]
        prob = round(random.random(), 2)
        return classifier_code, prob

    def predict_whole_frame(self, p, mediaobj, **kwargs):
        """
        Overridden method: predict label for full frame (OPTIONAL, otherwise ignored)
        """
        classifier_code = random.sample(self.labels, 1)[0]
        prob = round(random.random(), 2)
        return classifier_code, prob


if __name__ == "__main__":
    # Get arguments from CLI
    args = api_argparser.parse_args()

    # Create classifier object
    sq_classifier = RandomClassifier(
        args.media_cache_dir, api_token=args.api_key, url=args.url, algname=args.algname, prob_thresh=args.prob_thresh
    )
    sq_classifier.build_classid_translator(asid=args.asid, info_key="code_short", class_labels=sq_classifier.labels)
    new_annotation_set = sq_classifier.run(args.asid)
