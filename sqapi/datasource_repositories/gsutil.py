import subprocess
from .datasource import DataSource, argparser, register_datasource_plugin


class GSUtil(DataSource):
    def list_object_paths(self, url):
        cmd = ['gsutil', 'ls', '-l', url]
        result = subprocess.run(cmd, stdout=subprocess.PIPE)
        lines = result.stdout.decode("utf-8").split("\n")
        objects = []
        for l in lines:
            params = l.split()
            if len(params) == 3:
                objects.append(dict(
                    path=self.get_object_path(params[2]),
                    basename=self.get_object_basename(params[2]),
                    type="file",
                    mtime=params[1],
                    size=params[0]
                ))
            elif len(params) == 1:
                objects.append(dict(
                    path=self.get_object_path(params[0]),
                    basename=self.get_object_basename(params[0]),
                    type="file",
                    mtime=None,
                    size=None
                ))
        return objects
        # return [self.get_object_path(u) for u in urls if u]


# Register datasource plugin
register_datasource_plugin(GSUtil)


if __name__ == "__main__":
    args = argparser.parse_args()
    ds = GSUtil(**vars(args))

    # process all data files
    # We would normally replace input arg with function to process object, but in this case, we're just printing
    ds.process_all_datafiles(func=None)
