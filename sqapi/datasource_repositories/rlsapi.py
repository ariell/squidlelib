from datetime import datetime, timedelta
import traceback
import atexit

import os
import requests
import pandas as pd
from parse import parse
import tempfile
from sqapi.datasource_repositories.datasource import DataSource, argparser, SafeWildcardDict, register_datasource_plugin





# CONSTANTS
PATHSEP = "$"
CSV_DATA_URL = "http://geoserver-rls.imas.utas.edu.au/geoserver/RLS/ows?service=WFS&version=1.0.0&request=" \
               "GetFeature&typeName=RLS%3ASurveyList&outputFormat=csv"
CSV_DATA_OVERRIDE_FILE = os.path.join(os.path.dirname(__file__), "SurveyListOverride.csv")
# DATABASE_URL = "/Users/ariell/Downloads/SurveyList.csv"


DATETIMECOL_FMT = "%Y-%m-%dT%H:%M:%S"
CAMPAIGNCOL_NAME = "__campaign__"
DATAPATH_PATTERN = PATHSEP+"{campaign}"+PATHSEP
DATAFILE_URL = "http://rls.tpac.org.au/pq/{deployment[basename]}/"
TEMPDIR = None

# FILTERS TO APPLY ON DATABASE AFTER DOWNLOAD
FILTERS = {
    "HasPQs": "Yes",
    # "StateArea": "Victoria"    # TODO: remove this temporary filter - just for initial imports
}


class RLSAPI(DataSource):
    def __init__(self, *args, **kw):
        super().__init__(*args, **kw)
        self.csvdatadf = None
        self.pathsep = PATHSEP

        # Set up a tempdir and delete it when done
        self.tempdir = tempfile.mkdtemp()
        # Register method to run on exit to delete the tempdir
        atexit.register(clear_tempfiles, self.tempdir)
        print("Created temp directory: {}".format(self.tempdir))

    def list_object_paths(self, url):
        # url = platform_data['datasource_pattern'].format(prefix=prefix)
        # print(f"list_path_objects: {path+'/'}")
        if not url.endswith(self.pathsep):
            url += self.pathsep

        try:
            p = parse(DATAPATH_PATTERN, url)
            campaign = p.named.get("campaign") if p else None
            df = self.get_csv_data(CSV_DATA_URL)

            if campaign is None:
                # print(f"Getting campaign: {url}")
                return [dict(
                    path="{}{}{}".format(url,i,self.pathsep),
                    basename=str(i),
                    type="dir",
                    mtime=None,
                    size=None
                ) for i in list(df[CAMPAIGNCOL_NAME].unique())]
            else:
                # print(f"\nGetting deployments: \nURL: {url} | CAMPAIGN: {campaign}\n")
                filt_df = df[df[CAMPAIGNCOL_NAME] == campaign]
                return [dict(
                    path="{}{}{}".format(url,r["SurveyID"],self.pathsep),
                    basename=str(r["SurveyID"]),
                    type="dir",
                    mtime=None,
                    size=None
                ) for i, r in filt_df.iterrows()]
        except Exception as e:
            traceback.print_exc()
            return []

    def get_csv_data(self, url):
        """
        Download csv file database for local processing
        :param url:
        :return:
        """
        if self.csvdatadf is None:
            if os.path.isfile(CSV_DATA_OVERRIDE_FILE):
                url = CSV_DATA_OVERRIDE_FILE
                print("Loading override data file: {}".format(CSV_DATA_OVERRIDE_FILE))
            else:
                print("Downloading data file: {}...".format(url))
            df = pd.read_csv(url)
            print("Got {} rows".format(df.shape[0]))
            for k,v in FILTERS.items():
                df = df[df[k] == v]
                print("Filtered: {}={} ({} rows)".format(k, v, df.shape[0]))
            df[CAMPAIGNCOL_NAME] = "RLS_" + df["Location"] + "_" + \
                               df["SurveyDate"].map(lambda x: str(datetime.strptime(x, DATETIMECOL_FMT).year))
            self.csvdatadf = df
        return self.csvdatadf

    def get_deployment_assets(self, deployment=SafeWildcardDict(), **kwargs):
        """
        Override method to generate temporary deployment metadata file from JSON endpoint.
        :param deployment:
        :param kwargs:
        :return:
        """
        # get survey record from cached database
        dpl_info = self.csvdatadf.loc[self.csvdatadf['SurveyID'].astype(str) == deployment.get("basename")] \
            .to_dict(orient="records")[0]

        dpl_datetime = datetime.strptime(dpl_info.get('SurveyDate'), DATETIMECOL_FMT)

        # update deployment name to include site name
        deployment["name"] = "{}_{}_{}".format(
            dpl_info.get('SurveyID'),
            dpl_info.get('SiteCode'),
            dpl_datetime.strftime('%Y-%m-%d')
        )

        # create dpl object
        dpl = super().get_deployment_assets(deployment=deployment, **kwargs)

        # Generate temporary metadata file through RLS API query
        datafile_path = dpl.datafiles[0].get("url")
        datafile_url = DATAFILE_URL.format(deployment=deployment)
        r = requests.get(datafile_url, headers={"Accept": "application/json"})
        if r.ok:
            df = pd.DataFrame.from_dict(r.json().get("results"))
            df.drop(["height", "width", "scaled", "urls"], inplace=True, axis=1)
            for i, r in df.iterrows():
                df.at[i, "depth"] = dpl_info.get("Depth", None)
                df.at[i, "alt"] = dpl_info.get("Altitude", None)
                # add fake offset so that images have sequenced timestamps
                df.at[i, "timestamp"] = (dpl_datetime+timedelta(seconds=i)).strftime(DATETIMECOL_FMT)
            df.to_csv(datafile_path, index=None)
        return dpl


# Register method to cleanup upon exit
def clear_tempfiles(tempdir):
    """
    Remove temporary files
    :return:
    """
    from shutil import rmtree
    print("Removing temporary files: {}".format(tempdir))
    rmtree(tempdir, ignore_errors=True)

    if os.path.isfile(CSV_DATA_OVERRIDE_FILE):
        print("Removing override survey list file: {}".format(CSV_DATA_OVERRIDE_FILE))
        os.remove(CSV_DATA_OVERRIDE_FILE)


# Register datasource plugin
register_datasource_plugin(RLSAPI)


if __name__ == "__main__":
    args = argparser.parse_args()
    ds = RLSAPI(**vars(args))

    # process all data files
    # We would normally replace input arg with function to process object, but in this case, we're just printing
    ds.process_all_datafiles(func=None)

    # # other examples:
    # # list all campaigns
    # campaigns = ds.list_campaigns()
    # print(campaigns)
    #
    # # list all deployments
    # alldeployments = ds.list_deployments()
    # print(alldeployments)
    #
    # # list deployments from specific campaign
    # deployments = ds.list_deployments(campaign=campaigns[0])
    # print(deployments)
    #
    # # list all data files
    # alldatafiles = ds.get_datafiles()
    # print(alldatafiles)
    #
    # # list datafiles from specific campaign/deployment
    # datafiles = ds.get_datafiles(campaign=campaigns[0], deployment=deployments[0])
    # print(datafiles)
