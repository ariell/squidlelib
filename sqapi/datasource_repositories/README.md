# Data sources

## Defining a data source
Datasources are defined from the base class
`sqapi.datasource.DataSource`. In most cases it will only be necessary
to define/override the `@abstractmethod: list_object_paths` which defines
how to list all objects within a defined url to a repository directory.
It should return a list of paths for the repository (not absolute urls).

See [datasource.py](datasource.py) for more info.


The following parameters need to be set for defining a datasource:
```
  --urlbase_browse URLBASE_BROWSE
                        Url base pattern to list repository objects, eg:
                        http://path/to/browse/{path}
  --campaign_search CAMPAIGN_SEARCH
                        Campaign path pattern. eg: *
  --deployment_search DEPLOYMENT_SEARCH
                        Deployment path pattern. eg:
                        {campaign[path]}platform/r*
  --datafile_search DATAFILE_SEARCH
                        [Optional] Data file path pattern to match. eg:
                        {deployment[path]}data/nav_*.csv
  --mediadir_search MEDIADIR_SEARCH
                        [Optional] Media dir pattern to match. eg:
                        {deployment[path]}i*_gtif
  --thumbdir_search THUMBDIR_SEARCH
                        [Optional] Thumbnail dir pattern match. eg:
                        {deployment[path]thumbs}
  --datafile_pattern DATAFILE_PATTERN
                        Url base pattern to create file link, eg: http://path.
                        to.download/{campaign[basename]}/data/{deployment[base
                        name]}/{search[basename]}
  --media_pattern MEDIA_PATTERN
                        Media path pattern to match. eg: http://base.url/{depl
                        oyment[path]}/{search[basename]}/{media[key]}
  --thumb_pattern THUMB_PATTERN
                        Thumbnail path pattern to match. eg: http://base.url/{
                        deployment[path]}/{search[basename]}/{media[key]}
```


## Examples

#### GCLOUD bucket
Assuming data structure: `https://imos-data.s3-ap-southeast-2.amazonaws.com/?delimiter=/&prefix=IMOS/AUV/{campaign}/{deployment}/`,
Get all datafiles matching `DATA_{campaign}_{deployment}.csv` by querying campaigns matching
pattern `*` from deployments matching `r*`, then construct datafile path as
`https://s3-ap-southeast-2.amazonaws.com/imos-data/IMOS/AUV/auv_viewer_data/csv_outputs/{campaign}/DATA_{campaign}_{deployment}.csv`:

```
# UOS tunasand data on SOI GCLOUD
datasource.urlbase_download = "http://soi-uos-data.storage.googleapis.com/{path}"

python -m sqapi.datasource_repositories.gcloud \
    --urlbase_browse 'https://www.googleapis.com/storage/v1/b/soi-uos-data/o?delimiter=/&prefix={path}' \
    --deployment_search '{campaign[path]}/tuna*/*' \
    --campaign_search '*' \
    --datafile_search '{deployment[path]}/stereo_pose_est.data' \
    --mediadir_search '{deployment[path]}/images' \
    --datafile_pattern 'http://soi-uos-data.storage.googleapis.com/{search[path]}' \
    --media_pattern '{search[path]}{{media.key}}'
```

See [gcloud.py](gcloud.py) for more info.

#### AWS S3
Get all datafiles matching `DATA_{campaign}_{deployment}.csv` by querying campaigns matching
pattern `*` from deployments matching `r*`, then construct datafile path as
`https://s3-ap-southeast-2.amazonaws.com/imos-data/IMOS/AUV/auv_viewer_data/csv_outputs/{campaign}/DATA_{campaign}_{deployment}.csv`:
```
# IMOS AUV on AWS S3 through AODN
datasource.urlbase_download = "https://s3-ap-southeast-2.amazonaws.com/imos-data/IMOS/AUV/auv_viewer_data/{path}"

python -m sqapi.datasource_repositories.aws \
    --urlbase_browse 'https://imos-data.s3-ap-southeast-2.amazonaws.com/?delimiter=/&prefix={path}' \
    --deployment_search '{campaign[path]}/r*' \
    --campaign_search 'IMOS/AUV/*' \
    --datafile_pattern 'https://s3-ap-southeast-2.amazonaws.com/imos-data/IMOS/AUV/auv_viewer_data/csv_outputs/{campaign[name]}/DATA_{campaign[name]}_{deployment[name]}.csv' \
    --media_pattern 'images/{campaign[basename]}/{deployment[basename]}/full_res/{{media.key}}' \
    --thumb_pattern 'images/{campaign[basename]}/{deployment[basename]}/images/{{media.key}}' \
#    --mediadir_search 'IMOS/AUV/auv_viewer_data/images/{campaign[name]}/{deployment[name]}/full_res'  \
#    --datafile_search 'IMOS/AUV/auv_viewer_data/csv_outputs/{campaign[name]}/DATA_{campaign[name]}_{deployment[name]}.csv' \
#    --datafile_pattern 'https://s3-ap-southeast-2.amazonaws.com/imos-data/{search[path]}' \
```

Note `datafile_search` is not set as `datafile_url` is fully defined
with deployment and campaign.
See [aws.py](aws.py) for more info.


#### GSUTIL command line client
```
datasource.urlbase_download = 'http://soi-uos-data.storage.googleapis.com/{path}'

python -m sqapi.datasource_repositories.gsutil \
    --urlbase_browse 'gs://soi-uos-data/{path}' \
    --deployment_search '{campaign[path]}/tuna*/*' \
    --campaign_search '*' \
    --datafile_search '{deployment[path]}/stereo_pose_est.data' \
    --mediadir_search '{deployment[path]}/images' \
    --datafile_pattern 'http://soi-uos-data.storage.googleapis.com/{search[path]}' \
    --media_pattern '{search[path]}{{media.key}}'
#    --thumb_pattern
```

#### Local file system:
Get all datafiles matching `*.data` by querying campaigns matching
pattern `ssk*` from deployments in subdirs matching `tuna*`:
```
# UOS tunasand data on local file system
datasource.urlbase_download = 'http://url/to/serve/files/{path}'

python -m sqapi.datasource_repositories.localfs \
    --urlbase_browse ~/Documents/data/university-southampton-squidle/{path} \
    --campaign_search 'ssk*' \
    --deployment_search '{campaign[path]}/tuna*/*' \
    --datafile_search '{deployment[path]}/*.data' \
    --mediadir_search '{deployment[path]}/images*' \
    --datafile_pattern '~/Documents/data/university-southampton-squidle/{search[path]}' \
    --media_pattern '{search[path]}{{media.key}}'
#    --thumb_pattern ''
```
Where `~/Documents/data/university-southampton-squidle` is the base
directory location of the data on the local machine and
`http://url/to/serve/files/` is the base url for accessing the files via
http. See [localfs.py](localfs.py) for more info.


#### REEF LIFE Survey:
```
# RLS API
datasource.urlbase_download = 'http://rls.tpac.org.au/pq/{path}'

python -m sqapi.datasource_repositories.rlsapi \
    --urlbase_browse {path} \
    --campaign_search '*' \
    --deployment_search '{campaign[path]}*' \
    --datafile_pattern '{self[tempdir]}/{deployment[basename]}.csv' \
    --media_pattern '{deployment[basename]}/{{media.key}}/' \
    --thumb_pattern '{deployment[basename]}/scale/800/{{media.key}}/'
#    --datafile_search  \
#    --mediadir_search  \
#    --thumbdir_search  \
```


#### HTTP directory listing
Get all datafiles matching `*_Metadata.txt` by querying campaigns matching
pattern `*` from deployments matching `*BRUVs`:
```
# BRUVs data hosted with HTTP directory on SQ+
datasource.urlbase_download = 'http://squidle.org/static/media/globalarchive/stereo-BRUVs/{path}'

python -m sqapi.datasource_repositories.httpdir \
    --urlbase_browse 'http://squidle.org/static/media/globalarchive/stereo-BRUVs/{path}' \
    --deployment_search '{campaign[path]}/*BRUVs' \
    --campaign_search '*' \
    --datafile_search '{deployment[path]}/*_Metadata.txt' \
    --mediadir_search '{deployment[path]}/images' \
    --datafile_pattern 'http://squidle.org/static/media/globalarchive/stereo-BRUVs/{search[path]}' \
    --media_pattern '{search[path]}{{media.key}}'
#    --thumb_pattern
```
```
datasource.urlbase_download =

python -m sqapi.datasource_repositories.httpdir \
    --urlbase_browse 'http://squidleold.acfr.usyd.edu.au/data/DIVER-selected-release/{path}' \
    --urlbase_download 'http://squidleold.acfr.usyd.edu.au/data/DIVER-selected-release/{path}' \
    --deployment_search '{campaign[path]}/r*' \
    --campaign_search '*' \
    --datafile_search '{deployment[path]}/renav/stereo_pose_est.data' \
    --mediadir_search '{deployment[path]}/renav/mesh/mosaic/mosaic-tiles'
    --datafile_pattern
    --media_pattern
    --thumb_pattern
```
See [httpdir.py](httpdir.py) for more info.


