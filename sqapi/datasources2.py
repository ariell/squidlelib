import timeit
import sys
import os
import json
import logging
from werkzeug.utils import secure_filename
from .users import SQAPIUser, api_argparser
from .ui import UIComponents

# Register datasource modules
from .datasource_repositories import *
from .datasource_repositories.datasource import DATASOURCE_REGISTRY


class SQAPIDatasources(SQAPIUser):
    platform_resource = "platform"
    campaign_resource = "campaign"
    deployment_resource = "deployment"
    campaign_file_resource = "campaign_file"
    media_save_resource = "media/save"

    def get_platform_ui(self, action=None):
        """

        :param action:
        :return:
        """
        data = self.get(resource=self.platform_resource, single=False, validate_api_output=False,
                        order_by=[{"field": "created_at", "direction": "desc"}])
        data_list = data.get('objects', [])
        platform, action = self.select_action_ui(data_list, action=action, resource="Platform", listfield="name")
        # action = self.select_action_ui(action=action, resource="Platform")
        if action == "create":
            user = self.get_user(token=self.api_token)
            platform_params = ["name", "description", "reference"]
            datasource_types = DATASOURCE_REGISTRY.keys()
            datasource_params = ["urlbase_browse", "campaign_search", "deployment_search", "datafile_pattern",
                                 "datafile_search", "media_pattern", "mediadir_search", "thumb_pattern",
                                 "thumbdir_search"]

            platform_info = UIComponents.input_multi("Enter platform data", platform_params)
            datasource_paths = UIComponents.input_multi("Enter datasource info", datasource_params)
            datafile_operations = UIComponents.input_json("File operations (as json)")
            datasource_type, idx = UIComponents.select_list("Choose a DATASOURCE TYPE:", datasource_types)
            platform_data = dict(datasource_paths=datasource_paths, datafile_operations=datafile_operations,
                                 datasource_type=datasource_type)

            return self.new_platform(platform_info.get("name"), platform_info.get("description"), user['id'],
                                     platform_data)
        elif action == "update":
            platform = self.select_list_gui("Choose a PLATFORM:", data_list, listfield="name")
            data = UIComponents.input_multi("Edit Platform", platform, exclude=["id", "created_at", "user_id"])
            self.update(data, resource=self.platform_resource, id=platform.get('id'))
        elif action == "delete":
            platform = self.select_list_gui("Choose a PLATFORM:", data_list, listfield="name")
            if input("***************************************************\n"
                     "Are you 100% certain that you want to delete the '{}' platform?\n"
                     "ALL deployments, media and associated annotations will also be deleted. \n"
                     "This is NOT reversible! \n"
                     "***************************************************\n"
                     "If so, type 'YES' => ".format(platform['name'])) == "YES":
                self.delete(resource=self.platform_resource, id=platform['id'])
        else:
            return platform
        return self.get_platform_ui()

    def get_campaign_ui(self, platform_id, user_id, action=None):
        """

        :param platform_id:
        :param user_id:
        :param action:
        :return:
        """
        data = self.get(resource=self.campaign_resource, single=False, validate_api_output=False,
                        results_per_page=1000, filters=[{'name': "platform_id", 'op': "eq", 'val': platform_id}],
                        order_by=[{"field": "created_at", "direction": "desc"}])
        data_list = data.get('objects', [])
        campaign, action = self.select_action_ui(data_list, action=action, resource="Campaign", listfield="name")
        if action == "create":
            raise NotImplemented("Manual Campaign creation has not been implemented yet!")
        elif action == "update":
            campaign = self.select_list_gui("Choose a CAMPAIGN to {}:".format(action), data_list, listfield="name")
            data = UIComponents.input_multi("Edit CAMPAIGN", campaign, exclude=["id", "created_at", "user_id",
                                                                                "platform_id", "file_count",
                                                                                "deployment_count"])
            self.update(data, resource=self.campaign_resource, id=campaign.get('id'))
        elif action == "delete":
            campaign = self.select_list_gui("Choose a CAMPAIGN to {}:".format(action), data_list, listfield="name")
            if input("***************************************************\n"
                     "Are you 100% certain that you want to delete the '{}' campaign?\n"
                     "ALL deployments, media and associated annotations will also be deleted. \n"
                     "This is NOT reversible! \n"
                     "***************************************************\n"
                     "If so, type 'YES' => ".format(campaign['name'])) == "YES":
                self.delete(resource=self.campaign_resource, id=campaign['id'])
        else:
            return campaign
        return self.get_campaign_ui(platform_id, user_id)

    def get_deployment_ui(self, campaign_id, user_id, action=None):
        """

        :param campaign_id:
        :param user_id:
        :param action:
        :return:
        """
        data = self.get(resource=self.deployment_resource, single=False, validate_api_output=False,
                        results_per_page=1000, filters=[{'name': "campaign_id", 'op': "eq", 'val': campaign_id}],
                        order_by=[{"field": "created_at", "direction": "desc"}])
        data_list = data.get('objects', [])
        deployment, action = self.select_action_ui(data_list, action=action, resource="Deployment", listfield="name")
        if action == "create":
            raise NotImplemented("Manual Deployment creation has not been implemented yet!")
        elif action == "update":
            deployment = self.select_list_gui("Choose a Deployment", options=data_list, listfield="name")
            data = UIComponents.input_multi("Edit DEPLOYMENT", deployment, exclude=["id", "campaign", "user_id",
                                                                                    "campaign_id", "media_count",
                                                                                    "latlon", "timestamp",
                                                                                    "created_at"])
            self.update(data, resource=self.deployment_resource, id=deployment.get('id'))
        elif action == "delete":
            deployment = self.select_list_gui("Choose a Deployment", options=data_list, listfield="name")
            if input("***************************************************\n"
                     "Are you 100% certain that you want to delete the '{}' deployment?\n"
                     "ALL media and associated annotations will also be deleted. \n"
                     "This is NOT reversible! \n"
                     "***************************************************\n"
                     "If so, type 'YES' => ".format(deployment['name'])) == "YES":
                self.delete(resource=self.deployment_resource, id=deployment['id'])
        else:
            return deployment
        return self.get_deployment_ui(campaign_id, user_id)

    def select_action_ui(self, list, options=None, action=None, resource="", listfield="name"):
        """

        :param list:
        :param options:
        :param action:
        :param resource:
        :param listfield:
        :return:
        """
        if options is None:
            options = [
                # {'action': "select", listfield: "✓ VIEW {}".format(resource)},
                {'action': "create", listfield: "✚ CREATE NEW {} (manual)".format(resource)},
                {'action': "update", listfield: "✎ EDIT {}".format(resource)},
                {'action': "delete", listfield: "✖ DELETE {}".format(resource)}
            ]
        if action is None:
            o = self.select_list_gui("Choose a {} / action:".format(resource), list+options, listfield=listfield)
            return o, o.get("action")
        elif action is False or action in [o.get("action") for o in options]:
            o = self.select_list_gui("Choose a {} to {}:".format(resource, action), list, listfield=listfield)
            return o, action
        else:
            raise ValueError("Invalid action ")

    def select_list_gui(self, title, options=[], multi_select=False, min_selection_count=1, listfield=None):
        """

        :param title:
        :param options:
        :param multi_select:
        :param min_selection_count:
        :param listfield:
        :return:
        """
        optionlist = [o[listfield] for o in options] if listfield is not None else options
        if len(options) > 0:
            opt, ind = UIComponents.select_list(title, optionlist, multi_select=multi_select, min_selection_count=min_selection_count)
            sys.stdout.flush()
            return options[ind]
        else:
            raise ValueError("No options found for '{}'".format(title))

    def get_campaign(self, campaign_key=None, user_id=None, id=None):
        """

        :param campaign_key:
        :param user_id:
        :param id:
        :return:
        """
        if id is not None:
            return self.get(resource=self.campaign_resource, id=id)
        elif campaign_key is not None and user_id is not None:
            filters = [{"name": "key", "op": "==", "val": campaign_key},
                       {"name": "user_id", "op": "==", "val": user_id}]
            return self.get(resource=self.deployment_resource, filters=filters, single=True)
        else:
            raise AssertionError("No ID or (campaign_key and user_id) found.")
        pass

    def get_deployment(self, deployment_key=None, campaign_id=None, id=None):
        """

        :param deployment_key:
        :param campaign_id:
        :param id:
        :return:
        """
        if id is not None:
            return self.get(resource=self.deployment_resource, id=id)
        elif deployment_key is not None and campaign_id is not None:
            filters = [{"name": "key", "op": "==", "val": deployment_key},
                       {"name": "campaign_id", "op": "==", "val": campaign_id}]
            return self.get(resource=self.deployment_resource, filters=filters, single=True)
        else:
            raise AssertionError("No ID or (deployment_key and campaign_id) found.")

    def get_last_deployment(self):
        """

        :return:
        """
        order_by = [{"field": "created_at", "direction": "desc"}]
        return self.get(resource=self.deployment_resource, order_by=order_by, limit=1, single=True)

    def get_last_campaign(self):
        """

        :return:
        """
        order_by = [{"field": "created_at", "direction": "desc"}]
        return self.get(resource=self.campaign_resource, order_by=order_by, limit=1, single=True)

    def get_create_campaign(self, campaign_name, user_id, platform_id, campaign_key=None, media_path="", thm_path=None):
        """

        :param campaign_name:
        :param user_id:
        :param platform_id:
        :param campaign_key:
        :param media_path:
        :param thm_path:
        :return:
        """
        if campaign_key is None:
            campaign_key = secure_filename(campaign_name)
        data = {
            'name': campaign_name,
            'key': campaign_key,
            'user_id': user_id,
            'platform_id': platform_id,
            'media_path_pattern': media_path,
            'thm_path_pattern': thm_path
        }
        check_fields = ["user_id", "key", "platform_id"]
        endpoint = self.build_resource_endpoint(self.campaign_resource)
        return self.get_or_create(endpoint, data, check_fields)

    def get_create_deployment(self, deployment_name, user_id, campaign_id, deployment_key=None):
        """

        :param deployment_name:
        :param user_id:
        :param campaign_id:
        :param deployment_key:
        :return:
        """
        if deployment_key is None:
            deployment_key = secure_filename(deployment_name)
        # timestamp = "{}".format(datetime.strptime(deployment_key[1:16], "%Y%m%d_%H%M%S"))
        data = {
            'name': deployment_name,
            'key': deployment_key,
            # 'timestamp': timestamp,
            'user_id': user_id,
            'campaign_id': campaign_id
        }
        check_fields = ["campaign_id", "key"]
        endpoint = self.build_resource_endpoint(self.deployment_resource)
        return self.get_or_create(endpoint, data, check_fields)

    def new_media_item(self, pose, timestamp, posedata, deployment_id=None, path=None, key=None, path_thm=None,  media_type="image"):
        """

        :param pose:
        :param timestamp:
        :param posedata:
        :param deployment_id:
        :param path:
        :param key:
        :param path_thm:
        :param media_type:
        :return:
        """
        data = {
            "path": path,
            "pose": pose,
            "timestamp": timestamp,
            "posedata": posedata,
            "media_type": media_type
        }
        if key is not None:
            data['key'] = key
        if path is not None:
            data['path'] = path
        if path_thm is not None:
            data['path_thm'] = path_thm
        if deployment_id is not None:
            data['deployment_id'] = deployment_id
        return self.create(data, resource=self.media_save_resource)

    def get_create_campaign_file(self, user_id, campaign_id, fileurl=None, fpath=None, force_create=False):
        """

        :param force_create:
        :param user_id:
        :param campaign_id:
        :param fileurl:
        :param fpath:
        :return:
        """
        assert fileurl is not None or fpath is not None, "Either 'fileurl' or 'fpath' need to be set"
        data = {
            "fileurl": fileurl,
            "description": "Auto uploaded using '{}' from: {}".format(os.path.basename(__file__), fileurl or fpath),
            "user_id": user_id,
            "campaign_id": campaign_id,
            "name": fileurl
        }
        if fileurl is not None:
            data["fileurl"] = fileurl
            data["name"] = fileurl
        elif fpath is not None:
            data["name"] = "{}/{}".format(campaign_id, os.path.basename(fpath))
        check_fields = ["campaign_id", "name"]
        endpoint = self.build_resource_endpoint(self.campaign_file_resource)
        if force_create:
            return self.create(data, endpoint + "/save", fpath=fpath), True
        else:
            return self.get_or_create(endpoint, data, check_fields, endpoint_create=endpoint+"/save", fpath=fpath)

    def run_file_operations(self, file_query, campaign_file, deployment_id):
        """

        :param file_query:
        :param campaign_file:
        :param deployment_id:
        :return:
        """
        print("    Running file operations")
        for op in file_query:
            if "operation" in op and op["operation"] == "df_to_dict":
                op["columns"]["deployment_id"] = {"ref": "literal", "value": deployment_id}
        query_url = self.base_url+campaign_file["fileurl"]+"?format=text&queryparams="+json.dumps(file_query)
        return self.get(query_url, validate_api_output=False)

    def import_deployment(self, platform, user, dpl):
        """

        :param platform:
        :param user:
        :param dpl:
        :return:
        """
        status = 0
        print("Checking deployment: {} > {}".format(dpl.campaign.get("name"), dpl.deployment.get("name")))

        user_id = user['id']
        platform_id = platform["id"]
        file_query = platform['data']['datafile_operations']
        media_path = dpl.mediadirs[0].get("url")
        thumb_path = dpl.thumbdirs[0].get("url") if len(dpl.thumbdirs) > 0 else None
        for f in dpl.datafiles:
            if os.path.isfile(f.get("url")):
                fpath = f.get("url")
                furl = None
            elif self.is_valid_url(f.get("url")):
                furl = f.get("url")
                fpath = None
            else:
                raise FileNotFoundError("Data file cannot be found. Must be a url or file path. {}".format(f.get("url")))
            # Get campaign
            campaign, new_campaign = self.get_create_campaign(
                dpl.campaign.get("name"), user_id, platform_id, campaign_key=dpl.campaign.get("key"),
                media_path=media_path, thm_path=thumb_path
            )

            # get / create deployment
            deployment, new_deployment = self.get_create_deployment(
                dpl.deployment.get("name"), user_id, campaign['id'], deployment_key=dpl.deployment.get("key")
            )

            print(deployment)

            if new_deployment:
                # Upload data file
                start_time = timeit.default_timer()
                campaign_file, new_campaign_file = self.get_create_campaign_file(user_id, campaign['id'], fpath=fpath, fileurl=furl, force_create=True)
                if new_campaign_file:
                    logging.info("INFO: Uploaded : {} in {}s".format(fpath, timeit.default_timer() - start_time))

                # generate filequery
                start_time = timeit.default_timer()
                resp = self.run_file_operations(file_query, campaign_file, deployment["id"])
                logging.info("INFO: Completed fileops on {} in {}s: added (response: {})".format(
                    campaign_file["fileurl"], timeit.default_timer() - start_time, resp))
                print("    Done in {}s: (response: {})".format(timeit.default_timer() - start_time, resp))
                status = 1  # set to 1 to show new deployment was created
            else:
                print("    TODO: CHECK IF NEW DATA FILE AND UPDATE EXISTING DEPLOYMENT WITH NEW FILE")

        return status

    def new_platform(self, name, description, user_id, data):
        """

        :param name:
        :param description:
        :param user_id:
        :param data:
        :return:
        """
        payload = {
            "name": name,
            "description": description,
            "user_id": user_id,
            "data": data
        }
        #endpoint = self.build_resource_endpoint(self.platform_resource)
        p = self.create(payload, resource=self.platform_resource)
        sys.stdout.flush()
        return p

    # def edit_platform_data(self, platform):
    #     # Patch platform
    #     data = {"data": UIComponents.input_multi("Enter platform data", platform['data'])}
    #     #endpoint = self.build_resource_endpoint(self.platform_resource)
    #     return self.update(data, resource=self.platform_resource, id=platform['id'])

    def new_platform_data_gui(self, fieldnames):
        """

        :param fieldnames:
        :return:
        """
        fieldvalues = UIComponents.input_multi("Enter platform data", fieldnames)
        return fieldvalues

    # def new_platform_gui(self):
    #     user = self.get_user(token=self.api_token)
    #     platform_params = ["name", "description"]
    #     datasource_types = ["sqapi.datasource_repositories.aws.AWS", "sqapi.datasource_repositories.gcloud.GCloud",
    #                         "sqapi.datasource_repositories.httpdir.HTTPDir", "sqapi.datasource_repositories.gsutil.GSUtil",
    #                         "sqapi.datasource_repositories.localfs.LocalFS"]
    #     datasource_params = ["urlbase_browse", "urlbase_download", "campaign_pattern", "deployment_pattern",
    #                          "datafile_pattern", "mediadir_pattern", "thumbdir_pattern"]
    #
    #     platform_info = UIComponents.input_multi("Enter platform data", platform_params)
    #     datasource_paths = UIComponents.input_multi("Enter datasource info", datasource_params)
    #     datafile_operations = UIComponents.input_json("File operations (as json)")
    #     datasource_type, idx = UIComponents.select_list("Choose a DATASOURCE TYPE:", datasource_types)
    #     platform_data = dict(datasource_paths=datasource_paths, datafile_operations=datafile_operations, datasource_type=datasource_type)
    #
    #     return self.new_platform(platform_info.get("name"), platform_info.get("description"), user['id'], platform_data)

    def export_deployment(self, id, save_path=None, returns=(), filters=(), transform=None):
        """

        :param id:
        :param save_path:
        :param returns:
        :param filters:
        :param transform:
        :return:
        """
        valid_transforms = [None]
        valid_filters = []
        valid_returns = ["pose", "posedata", "mediapaths", "mediapaths-resolved", "datasetkeys", "events"]
        assert transform in valid_transforms, "Invalid 'transform' parameter. Can be one of: {}".format(valid_transforms)
        assert isinstance(filters, (list, tuple)), "Input parameter 'filters' must be a list/tuple."
        assert isinstance(returns, (list, tuple)), "Input parameter 'returns' must be a list/tuple."
        assert all([f in valid_filters for f in filters]), "Invalid 'filters' parameter. Can only be: {}".format(valid_filters)
        assert all([r in valid_returns for r in returns]), "Invalid 'returns' parameter. Can only be: {}".format(valid_returns)

        qsparams = [("format", "csv"), ("file", "attachment")]
        for f in filters:
            qsparams.append(("filter", f))

        for r in returns:
            qsparams.append(("return", r))

        if transform is not None:
            qsparams.append(("transform", transform))

        return self.download_file(resource=self.deployment_resource, id=id, append="/export", save_path=save_path, query_string_params=qsparams)


if __name__ == "__main__":
    options = [
        {'action': "sync_one", 'title': "✓ REMOTE SYNC Selected"},
        {'action': "sync_all", 'title': "↻ REMOTE SYNC ALL"},
        {'action': "edit", 'title': "✎ EDIT existing datasets"}
    ]

    logfile = "{}.log".format(os.path.realpath(__file__))
    logging.basicConfig(filename=logfile, level=logging.INFO, format='%(asctime)s %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p')

    args = api_argparser.parse_args()
    url = args.url  # get url from cmd line argument
    api_token = args.api_key  # get api_token from cmd line argument

    sqapi = SQAPIDatasources(api_token=api_token, url=url)

    user = sqapi.get_user(token=api_token)

    action = sqapi.select_list_gui("What would you like to do?", options, listfield="title").get("action")

    # EDIT
    if action == "edit":
        # Get platform
        platform = sqapi.get_platform_ui(action=None)
        # Get campaign
        campaign = sqapi.get_campaign_ui(platform.get("id"), user.get("id"), action=None)
        # Get deployment
        deployment = sqapi.get_deployment_ui(campaign.get("id"), user.get("id"), action=None)

        print("\n***********\nPlatform:\n{}\n***********\nCampaign:\n{}\n***********\nDeployment:\n{}"
              .format(json.dumps(platform, indent=2), json.dumps(campaign, indent=2), json.dumps(deployment, indent=2)))
    # Sync one
    elif action == "sync_one":
        # Get platform
        platform = sqapi.get_platform_ui(action=False)
        # Get datasource
        # ds = sqapi.get_datasource(platform.get("data"))
        platform_data = platform.get("data", {})
        datasource_type = platform_data.get("datasource_type")
        ds = DATASOURCE_REGISTRY.get(datasource_type)(validate_searchs=True, **platform_data.get("datasource_paths"))
        campaign = sqapi.select_list_gui("Choose a CAMPAIGN:", ds.list_campaigns(), listfield="name")
        deployment = sqapi.select_list_gui("Choose a CAMPAIGN:", ds.list_deployments(campaign=campaign), listfield="name")
        dpl = ds.get_deployment_assets(campaign=campaign, deployment=deployment)

        sqapi.import_deployment(platform, user, dpl)
        print(json.dumps(dpl.dict(), indent=2))
        #print(platform)

    elif action == "sync_all":
        # Get platform
        platform = sqapi.get_platform_ui(action=False)
        # Get datasource
        #ds = sqapi.get_datasource(platform.get("data"))
        platform_data = platform.get("data", {})
        datasource_type = platform_data.get("datasource_type")
        ds = DATASOURCE_REGISTRY.get(datasource_type)(validate_searchs=True, **platform_data.get("datasource_paths"))
        for c in ds.list_campaigns():
            for d in ds.list_deployments(campaign=c):
                try:
                    dpl = ds.get_deployment_assets(campaign=c, deployment=d)
                    sqapi.import_deployment(platform, user, dpl)
                    # print(f'{"*"*30}\n  {c.get("name")} > {d.get("name")}')
                    # print(json.dumps(dpl.dict(), indent=2))
                except Exception as e:
                    logging.error("Unable to import: {} > {}".format(c.get('name'), d.get('name')))




