from pick import pick
import json
import pprint
from pygments.lexers.data import JsonLexer
from prompt_toolkit.shortcuts import prompt
from prompt_toolkit.lexers import PygmentsLexer


class UIComponents(object):
    @staticmethod
    def select_list(title, optionlist, multi_select=False, min_selection_count=1):
        """

        :param title:
        :param optionlist:
        :param multi_select:
        :param min_selection_count:
        :return:
        """
        if not multi_select:
            option, index = pick(optionlist, title, multi_select=False, min_selection_count=min_selection_count)
            return option, index
        else:
            options = pick(optionlist, title, multi_select=True, min_selection_count=min_selection_count)
            return [(o[0], o[1]) for o in options]  # list of [(opt, ind)]

    @staticmethod
    def input_json(title, json_value=u""):
        """

        :param title:
        :param json_value:
        :return:
        """
        print ("\n* SET VALUE FOR: {}. \n  Paste JSON below. Hit ESC+ENTER or CTRL+o when done.\n".format(title))
        contents = prompt(u"> ", default=json_value, multiline=True, mouse_support=True, lexer=PygmentsLexer(JsonLexer))
        # contents = []
        # while True:
        #     try:
        #         line = raw_input("")
        #     except EOFError:
        #         break
        #     contents.append(line)
        print (contents)
        return json.loads(contents) if contents else None

    @staticmethod
    def input_multi(title, fields, exclude=[]):
        """

        :param title:
        :param fields:
        :param exclude:
        :return:
        """
        data = {}
        if isinstance(fields, list):
            fields = {f: None for f in fields}  # convert to dict
        for f in fields.keys():
            if exclude is None or f not in exclude:
                if isinstance(fields[f], (str,)) or fields[f] is None:
                    data[f] = prompt(u"\n* SET VALUE FOR: '{}'\n  > ".format(f), default=fields[f] or u"") or fields[f]
                elif isinstance(fields[f], (dict, list)):
                    data[f] = UIComponents.input_json(f, json.dumps(fields[f], indent=2))
                    #fields[f] = prompt(u"\n* SET VALUE FOR: '{}'\n  > ".format(f), default=json.dumps(fields[f]), multiline=True, mouse_support=True)  #, lexer=JsonLexer)
                else:
                    raise Exception(u"Unsupported field type for '{}'. Don't know how to handle type: {}.".format(f, type(fields[f])))
        return data