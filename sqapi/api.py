import json
import os
import re
import requests
import argparse
from posixpath import join as urljoin

try:
    from urllib import unquote
except ImportError as e:
    from urllib.parse import unquote

allowed_http_codes = [200, 201, 202, 204]

api_argparser = argparse.ArgumentParser()
api_argparser.add_argument("--api_key", type=str, help="The API key of the user to act on behalf of", required=True)
api_argparser.add_argument("--url", type=str, help="The base URL of the server (default: http://localhost:5000)", default="http://localhost:5000")


class SQAPIBase:
    api_token = ""
    base_url = "http://localhost:5000"
    api_url = "api"

    def __init__(self, api_token=None, url=None, **kwargs):
        """

        :param api_token:
        :param url:
        :param kwargs:
        """
        if api_token is not None:
            self.api_token = api_token
        if url is not None:
            self.base_url = url

    def build_resource_endpoint(self, resource):
        """

        :param resource:
        :return:
        """
        endpoint = urljoin(self.base_url, self.api_url, resource)
        return endpoint

    def build_resource_url(self, resource, **kwargs):
        """

        :param resource:
        :param kwargs:
        :return:
        """
        endpoint = self.build_resource_endpoint(resource)
        query = self.get_query(endpoint, **kwargs)
        return query.build_url()

    def get_query(self, endpoint=None, resource=None, id=None, append=None, filters=None, single=None, limit=None, results_per_page=None, order_by=None, page=None, query_string_params=None):
        """

        :param endpoint:
        :param resource:
        :param id:
        :param append:
        :param filters:
        :param single:
        :param limit:
        :param results_per_page:
        :param order_by:
        :param page:
        :param query_string_params:
        :return:
        """
        # TODO: validate input arguments
        # build endpoint from resource if endpoint is not set
        if endpoint is None and resource is not None:
            endpoint = self.build_resource_endpoint(resource)
        assert endpoint is not None, "No endpoint supplied. Either 'endpoint' or 'resource' must be set."

        query = SqQueryBuilder(endpoint, single=single).set_id(id).append(append).set_limit(limit).set_page(page)\
            .set_results_per_page(results_per_page)
        if filters is not None:
            query.set_filters(filters)
            # for f in filters:
            #     query.add_filter(**f)
        if order_by is not None:
            for o in order_by:
                query.add_sorter(**o)
        if query_string_params is not None:
            for q in query_string_params:
                query.add_qsparam(*q)
        return query

    def get(self, endpoint=None, resource=None, validate_api_output=True, **kwargs):
        """

        :param endpoint:
        :param resource:
        :param validate_api_output:
        :param kwargs:
        :return:
        """
        query = self.get_query(endpoint=endpoint, resource=resource, **kwargs)
        return query.get_json(api_token=self.api_token, validate_api_output=validate_api_output)

    def create(self, data, endpoint=None, resource=None, fpath=None, **kwargs):
        """

        :param fpath:
        :param data:
        :param endpoint:
        :param resource:
        :param kwargs:
        :return:
        """
        query = self.get_query(endpoint=endpoint, resource=resource, **kwargs)
        #return query.post_json(data, api_token=self.api_token)
        if fpath is None:
            return query.post_json(data, api_token=self.api_token)
        else:
            print(fpath)
            print(data)
            return query.post_file(fpath, data, api_token=self.api_token)

    def upload_file(self, fpath, data, endpoint=None, resource=None, **kwargs):
        """

        :param fpath:
        :param data:
        :param endpoint:
        :param resource:
        :param kwargs:
        :return:
        """
        query = self.get_query(endpoint=endpoint, resource=resource, **kwargs)
        return query.post_file(fpath, data, api_token=self.api_token)

    def update(self, data, id=None, endpoint=None, resource=None, **kwargs):
        """

        :param data:
        :param id:
        :param endpoint:
        :param resource:
        :param kwargs:
        :return:
        """
        assert id is not None, "ID is not specified"
        query = self.get_query(endpoint=endpoint, resource=resource, id=id, **kwargs)
        return query.patch_json(data, api_token=self.api_token)

    def delete(self, id=None, endpoint=None, resource=None, **kwargs):
        """

        :param id:
        :param endpoint:
        :param resource:
        :param kwargs:
        :return:
        """
        assert id is not None, "ID is not specified"
        query = self.get_query(endpoint=endpoint, resource=resource, id=id, **kwargs)
        return query.delete(api_token=self.api_token)

    def get_or_create(self, endpoint, data, id_fields, endpoint_create=None, fpath=None):
        """

        :param endpoint:
        :param data:
        :param id_fields:
        :param endpoint_create:
        :param fpath:
        :return:
        """
        if endpoint_create is None:
            endpoint_create = endpoint
        try:
            query = SqQueryBuilder(endpoint, single=True)
            for field in id_fields:
                query.add_filter(name=field, op="eq", val=data[field])
            return query.get_json(api_token=self.api_token), False
        except AssertionError as e:
            query = SqQueryBuilder(endpoint_create)
            if fpath is None:
                return query.post_json(data, api_token=self.api_token), True
            else:
                return query.post_file(fpath, data, api_token=self.api_token), True

    def download_file(self, endpoint=None, resource=None, save_path=None, **kwargs):
        """

        :param endpoint:
        :param resource:
        :param save_path:
        :param kwargs:
        :return:
        """
        # TODO: if save_path is None, rather return io buffer instead of assertion exception
        assert save_path is not None, "No 'save_path' set. You need to set a path to save the file."
        fname = None
        if not os.path.isdir(save_path):    # check if dir
            fname = os.path.basename(save_path)     # if not dir, assume file passed in and set file name
            save_path = os.path.dirname(save_path)      # set dir name
            assert os.path.isdir(save_path), "{} is not a valid directory".format(save_path)    # check dir exists

        query = self.get_query(endpoint=endpoint, resource=resource, **kwargs)
        data, inferred_fname, metadata = query.get_file(api_token=self.api_token)
        file_path = os.path.join(save_path, inferred_fname if fname is None else fname)
        open(file_path, 'wb').write(data)
        print (" - SAVED FILE: %s" % file_path)
        if metadata is not None:
            metadata_file_path = "%s-metadata.json" % file_path
            #print(metadata)
            open(metadata_file_path, 'w').write(json.dumps(json.loads(metadata), indent=4))
            print(" - SAVED METADATA FILE: %s" % metadata_file_path)

        return file_path

    def is_valid_url(self, url):
        """

        :param url:
        :return:
        """
        ret = requests.head(url)
        return ret.status_code in allowed_http_codes


class SqQueryBuilder:
    def __init__(self, endpoint, single=None, disjunction=None):
        """

        :param endpoint:
        :param single:
        :param disjunction:
        """
        self.endpoint = endpoint
        self.q = {}
        self.endpoint_suffix = ""
        self.querystring_params = []

        if single is not None:
            self.q["single"] = single
        if disjunction is not None:
            self.q["disjunction"] = disjunction

    def add_filter(self, name=None, op=None, val=None):
        """

        :param name:
        :param op:
        :param val:
        :return:
        """
        if "filters" not in self.q:
            self.q["filters"] = []
        self.q["filters"].append({"name": name, "op": op, "val": val})
        return self

    def set_filters(self, filters):
        self.q["filters"] = filters

    def add_sorter(self, field=None, direction="asc"):
        """

        :param field:
        :param direction:
        :return:
        """
        assert isinstance(field, str), "Parameter 'field' must be a string."
        assert direction in ["asc", "desc"], "Parameter 'direction' must be either ['asc','desc']."
        if "order_by" not in self.q:
            self.q["order_by"] = []
        self.q["order_by"].append({"field": field, "direction": direction})
        return self

    def set_results_per_page(self, num_results):
        """

        :param num_results:
        :return:
        """
        if num_results is not None:
            assert isinstance(num_results, int), "Limit parameter needs to be an integer"
            self.add_qsparam("results_per_page", num_results)
        return self

    def set_limit(self, num_results):
        """

        :param num_results:
        :return:
        """
        if num_results is not None:
            assert isinstance(num_results, int), "Limit parameter needs to be an integer"
            #self.add_qsparam("results_per_page", num_results)
            self.q["limit"] = num_results
        return self

    def set_page(self, page):
        """

        :param page:
        :return:
        """
        if page is not None:
            assert isinstance(page, int), "Page parameter needs to be an integer."
            self.add_qsparam("page", page)
        return self

    def add_qsparam(self, name, value):
        """

        :param name:
        :param value:
        :return:
        """
        self.querystring_params.append((name, value))
        return self

    def build_url(self):
        """

        :return:
        """
        url = self.endpoint+self.endpoint_suffix
        if bool(self.q):
            url += "?q=" + json.dumps(self.q, separators=(',', ':'))
        if bool(self.querystring_params):
            for p in self.querystring_params:
                joiner = "&" if "?" in url else "?"
                assert len(p) == 2, "Query parameter needs [name, value], found '{}'.".format(p)
                url += "{}{}={}".format(joiner, p[0], p[1])
        return url

    def set_id(self, id):
        """

        :param id:
        :return:
        """
        if id is not None:
            assert isinstance(id, int), "ID needs to be valid integer."
            self.append(id, joiner="/")
        return self

    def append(self, suffix, joiner=""):
        """

        :param suffix:
        :param joiner:
        :return:
        """
        # Append suffix if set, otherwise set it
        if suffix is not None:
            self.endpoint_suffix += "{}{}".format(joiner, suffix)
        return self

    def get_json(self, api_token=None, validate_api_output=True, build_url=True):
        """

        :param api_token:
        :param validate_api_output:
        :param build_url:
        :return:
        """
        url = self.build_url() if build_url else self.endpoint
        r = requests.get(url, headers={'auth-token': api_token, "Accept":"application/json"})
        r_json = self.get_response(r, "GET")
        #print "\n\n", r_json
        if validate_api_output:
            assert "id" in r_json or r_json.get("num_results", 0) > 0, "Invalid response or no results ({})".format(url)
        return r_json

    def post_json(self, data, api_token=None, build_url=False):
        """

        :param data:
        :param api_token:
        :param build_url:
        :return:
        """
        url = self.build_url() if build_url else self.endpoint
        r = requests.post(url, json=data, headers={'auth-token': api_token, "Accept":"application/json"})
        # print(" - POST JSON: {}".format(url))
        return self.get_response(r, "POST")

    def post(self, data, api_token, build_url=False):
        """

        :param data:
        :param api_token:
        :param build_url:
        :return:
        """
        url = self.build_url() if build_url else self.endpoint
        r = requests.post(url, data=data, headers={'auth-token': api_token, "Accept":"application/json"})
        # print(" - POST: {}".format(url))
        return self.get_response(r, "POST")

    def post_file(self, fpath, data, api_token, build_url=False):
        """

        :param fpath:
        :param data:
        :param api_token:
        :param build_url:
        :return:
        """
        url = self.build_url() if build_url else self.endpoint
        files = {'file': (os.path.basename(fpath), open(fpath,'rb'), 'text/x-spam')}
        r = requests.post(url, files=files, data=data, headers={'auth-token': api_token, "Accept": "application/json"})
        return self.get_response(r, "POST")

    def patch_json(self, data, api_token=None, build_url=True):
        """

        :param data:
        :param api_token:
        :param build_url:
        :return:
        """
        url = self.build_url() if build_url else self.endpoint
        r = requests.patch(url, json=data, headers={'auth-token': api_token, "Accept":"application/json"})
        #r = requests.patch(url, json=data, headers={'auth-token': api_token})
        # print(" - PATCH JSON: {}".format(url))
        return self.get_response(r, "PATCH")

    def delete(self, api_token=None, build_url=True):
        """

        :param api_token:
        :param build_url:
        :return:
        """
        url = self.build_url() if build_url else self.endpoint
        r = requests.delete(url, headers={'auth-token': api_token})
        print (" * DELETE {} ({}): {}".format(r.reason, r.status_code, unquote(r.url)))
        return r

    def get_file(self, api_token=None, build_url=True):
        """

        :param api_token:
        :param build_url:
        :return:
        """
        url = self.build_url() if build_url else self.endpoint
        r = requests.get(url, headers={'auth-token': api_token})
        data = self.get_response(r, "GET", is_json=False)
        fname = re.findall("filename=(.+)", r.headers['content-disposition'])[0]
        metadata = r.headers.get("X-Content-Metadata", None)
        return data, fname, metadata

    def get_response(self, r, method="HTTP_REQUEST", is_json=True):
        """

        :param r:
        :param method:
        :param is_json:
        :return:
        """
        assert r.status_code in allowed_http_codes, \
            "INVALID HTTP RESPONSE | code: {} | method: {} | reason: {} | url: {}".format(r.status_code, method, r.reason, unquote(r.url))
        if is_json:
            ret = r.json()
        else:
            ret = r.content
        print (" * {} {} ({}): {}".format(method, r.reason, r.status_code, unquote(r.url)))
        return ret
