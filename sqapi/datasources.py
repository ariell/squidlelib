# import httplib2
import timeit
from datetime import datetime

#import pandas as pd
#import easygui
import sys

import re
from bs4 import BeautifulSoup
import requests
import os
import xmltodict
import json
import logging
import glob

from .users import SQAPIUser, api_argparser
from .ui import UIComponents

#url = 'http://squidle.acfr.usyd.edu.au/images/'

# importdata_pattern = 'https://imos-data.s3-ap-southeast-2.amazonaws.com/?delimiter=/&prefix={prefix}'
# csvfile_pattern = 'https://s3-ap-southeast-2.amazonaws.com/imos-data/IMOS/AUV/auv_viewer_data/csv_outputs/{campaign}/DATA_{campaign}_{deployment}.csv'
# img_pattern = 'http://squidle.acfr.usyd.edu.au/images/{campaign}/{deployment}/images/{imgname}.png'
# thm_pattern = 'https://s3-ap-southeast-2.amazonaws.com/imos-data/IMOS/AUV/auv_viewer_data/thumbnails/{campaign}/{deployment}/i2jpg/{imgname}.jpg'


class SQAPIDatasources(SQAPIUser):
    platform_resource = "platform"
    campaign_resource = "campaign"
    deployment_resource = "deployment"
    campaign_file_resource = "campaign_file"
    media_save_resource = "media/save"

    def get_datasource_list(self, platform_data, urlkey=None):
        datasource_type = platform_data['datasource_type']
        if datasource_type == "aws":
            prefix = urlkey if urlkey is not None else platform_data['datasource_prefix']
            url = platform_data['datasource_pattern'].format(prefix=prefix)
            page = requests.get(url).text
            data = xmltodict.parse(page)
            try:
                return [i['Prefix'] for i in data['ListBucketResult']['CommonPrefixes']]
            except Exception as e:
                return []
        elif datasource_type == "http":
            url = urlkey if urlkey is not None else platform_data['datasource_pattern']
            page = requests.get(url).text
            soup = BeautifulSoup(page, 'html.parser')
            return [url + node.get('href') for node in soup.find_all('a')]  # if node.get('href').endswith(ext)]

        elif datasource_type == "local":
            path = urlkey+"/*" if urlkey is not None else platform_data['datasource_pattern']
            return glob.glob(path)

        elif datasource_type == "thredds":
            prefix = urlkey if urlkey is not None else platform_data['datasource_prefix']
            url = platform_data['datasource_pattern'].format(prefix=prefix)
            page = requests.get(url).text
            data = xmltodict.parse(page)
            try:
                return [i['Prefix'] for i in data['ListBucketResult']['CommonPrefixes']]
            except Exception as e:
                return []

    def select_platform(self):
        new_opt = "+ NEW PLATFORM"
        edit_opt = "# EDIT PLATFORM"
        del_opt = "x DELETE PLATFORM"
        resp = self.get(resource=self.platform_resource, single=False, validate_api_output=False, order_by=[{"field": "created_at", "direction": "desc"}])
        platforms = {i["name"]:i for i in resp['objects']}
        key, baskey = self.select_list_gui("Choose a PLATFORM:", list(platforms.keys()) + [new_opt, edit_opt, del_opt])
        if key == new_opt:
            return self.new_platform_gui()
        elif key == edit_opt:
            key, baskey = self.select_list_gui("Choose a PLATFORM to EDIT:", list(platforms.keys()))
            return self.edit_platform_data(platforms[key])
        elif key == del_opt:
            key, baskey = self.select_list_gui("Choose a PLATFORM to DELETE:", list(platforms.keys()))
            self.delete_platform(platforms[key])
            return self.select_platform()
        else:
            return platforms[key]

    def select_campaign(self):
        new_opt = "+ NEW CAMPAIGN"
        edit_opt = "# EDIT CAMPAIGN"
        del_opt = "x DELETE CAMPAIGN"
        resp = self.get(resource=self.campaign_resource, single=False, validate_api_output=False, results_per_page=1000)
        objects = {i["key"]: i for i in resp['objects']}
        key, baskey = self.select_list_gui("Choose a PLATFORM:", list(objects.keys()) + [new_opt, edit_opt, del_opt])
        if key == new_opt:
            raise NotImplemented("New campaign from menu not implemented yet!")
        elif key == edit_opt:
            raise NotImplemented("Edit campaign from menu not implemented yet!")
        elif key == del_opt:
            raise NotImplemented("Delete campaign from menu not implemented yet!")
        else:
            return objects[key]

    def select_list_gui(self, title, optionlist, multi_select=False, min_selection_count=1):
        option = UIComponents.select_list(title, optionlist, multi_select=multi_select, min_selection_count=min_selection_count)
        dirname = os.path.basename(os.path.normpath(option))
        sys.stdout.flush()
        return option, dirname

    def get_campaign(self, campaign_key=None, user_id=None, id=None):
        if id is not None:
            return self.get(resource=self.campaign_resource, id=id)
        elif campaign_key is not None and user_id is not None:
            filters = [{"name": "key", "op": "==", "val": campaign_key},
                       {"name": "user_id", "op": "==", "val": user_id}]
            return self.get(resource=self.deployment_resource, filters=filters, single=True)
        else:
            raise AssertionError("No ID or (campaign_key and user_id) found.")
        pass

    def get_deployment(self, deployment_key=None, campaign_id=None, id=None):
        if id is not None:
            return self.get(resource=self.deployment_resource, id=id)
        elif deployment_key is not None and campaign_id is not None:
            filters = [{"name": "key", "op": "==", "val": deployment_key},
                       {"name": "campaign_id", "op": "==", "val": campaign_id}]
            return self.get(resource=self.deployment_resource, filters=filters, single=True)
        else:
            raise AssertionError("No ID or (deployment_key and campaign_id) found.")

    def get_last_deployment(self):
        order_by = [{"field": "created_at", "direction": "desc"}]
        return self.get(resource=self.deployment_resource, order_by=order_by, limit=1, single=True)

    def get_last_campaign(self):
        order_by = [{"field": "created_at", "direction": "desc"}]
        return self.get(resource=self.campaign_resource, order_by=order_by, limit=1, single=True)

    def get_create_campaign(self, campaign_key, user_id, platform):
        if platform['data']['campaign_name_regex']:
            name_search = re.search(platform['data']['campaign_name_regex'], campaign_key)
            short_name = name_search.group(1) if name_search else campaign_key
        else:
            short_name = campaign_key
        data = {
            'name': short_name,
            'key': campaign_key,
            'user_id': user_id,
            'platform_id': platform['id'],
            'media_path_pattern': platform['data']['media_path_pattern'].format(
                campaign="{campaign.key}", deployment="{deployment.key}", imgname="{media.key}"
            ),
            'thm_path_pattern': platform['data']['thm_path_pattern'].format(
                campaign="{campaign.key}", deployment="{deployment.key}", imgname="{media.key}", imgid="{media.id}"
            ) if platform['data']['thm_path_pattern'] else None
        }
        check_fields = ["user_id", "key"]
        endpoint = self.build_resource_endpoint(self.campaign_resource)
        return self.get_or_create(endpoint, data, check_fields)

    def get_create_deployment(self, deployment_key, user_id, campaign_id, platform, name=None):
        if platform['data']['deployment_name_regex'] and name is None:
            name_search = re.search(platform['data']['deployment_name_regex'], deployment_key)
            name = name_search.group(1) if name_search else deployment_key
        elif name is None:
            name = deployment_key
        # timestamp = "{}".format(datetime.strptime(deployment_key[1:16], "%Y%m%d_%H%M%S"))
        data = {
            'name': name,
            'key': deployment_key,
            # 'timestamp': timestamp,
            'user_id': user_id,
            'campaign_id': campaign_id
        }
        check_fields = ["campaign_id", "key"]
        endpoint = self.build_resource_endpoint(self.deployment_resource)
        return self.get_or_create(endpoint, data, check_fields)

    def new_media_item(self, pose, timestamp, posedata, deployment_id=None, path=None, key=None, path_thm=None,  media_type="image"):
        data = {
            "path": path,
            "pose": pose,
            "timestamp": timestamp,
            "posedata": posedata,
            "media_type": media_type
        }
        if key is not None:
            data['key'] = key
        if path is not None:
            data['path'] = path
        if path_thm is not None:
            data['path_thm'] = path_thm
        if deployment_id is not None:
            data['deployment_id'] = deployment_id
        return self.create(data, resource=self.media_save_resource)

    def get_create_campaign_file(self, fileurl, user_id, campaign_id):
        data = {
            "fileurl": fileurl,
            "description": "Auto uploaded using '{}' from: {}".format(os.path.basename(__file__), fileurl),
            "user_id": user_id,
            "campaign_id": campaign_id,
            "name": fileurl
        }
        check_fields = ["campaign_id", "name"]
        endpoint = self.build_resource_endpoint(self.campaign_file_resource)
        endpoint_create = endpoint+"/save"
        return self.get_or_create(endpoint, data, check_fields, endpoint_create=endpoint_create)

    def run_file_operations(self, platform, campaign_file, deployment):
        print ("    Running file operations")
        file_query = platform['data']['datafile_operations']
        for op in file_query:
            if "operation" in op and op["operation"] == "df_to_dict":
                op["columns"]["deployment_id"] = {"ref": "literal", "value": deployment["id"]}
        query_url = self.base_url+campaign_file["fileurl"]+"?format=text&queryparams="+json.dumps(file_query)
        return self.get(query_url, validate_api_output=False)

    def import_deployment(self, platform, user, campaign_key, deployment_key):
        status = 0
        print ("Checking deployment: {} > {}".format(campaign_key, deployment_key))

        fileurl = platform["data"]['datafile_pattern'].format(campaign=campaign_key, deployment=deployment_key)
        if self.is_valid_url(fileurl):
            user_id = user['id']  # this also check the above was a success

            # Get campaign
            campaign, new_campaign = self.get_create_campaign(campaign_key, user_id, platform)

            # Upload campaign file
            start_time = timeit.default_timer()
            campaign_file, new_campaign_file = self.get_create_campaign_file(fileurl, user_id, campaign['id'])
            if new_campaign_file:
                logging.info("INFO: Uploaded : {} in {}s".format(fileurl, timeit.default_timer() - start_time))

            deployment, new_deployment = self.get_create_deployment(deployment_key, user_id, campaign['id'], platform)

            if not new_deployment:
                if new_campaign_file:
                    print("    TODO: UPDATE EXISTING DEPLOYMENT WITH NEW FILE")
                else:
                    print("    Deployment and CampaignFile already exists. No work done.")
            else:
                # generate filequery
                start_time = timeit.default_timer()
                resp = self.run_file_operations(platform, campaign_file, deployment)
                logging.info("INFO: Completed fileops on {} in {}s: added (response: {})".format(campaign_file["fileurl"], timeit.default_timer()-start_time, resp))
                print("    Done in {}s: (response: {})".format(timeit.default_timer()-start_time, resp))
                status = 1  # set to 1 to show new deployment was created
        else:
            logging.error("ERROR: File not found: {}".format(fileurl))
            status = -1     # return -1 for error

        return status

    def new_platform(self, name, description, user_id, data):
        payload = {
            "name": name,
            "description": description,
            "user_id": user_id,
            "data": data
        }
        #endpoint = self.build_resource_endpoint(self.platform_resource)
        p = self.create(payload, resource=self.platform_resource)
        sys.stdout.flush()
        return p

    def edit_platform_data(self, platform):
        # Patch platform
        data = {"data": UIComponents.input_multi("Enter platform data", platform['data'])}
        #endpoint = self.build_resource_endpoint(self.platform_resource)
        return self.update(data, resource=self.platform_resource, id=platform['id'])

    def delete_platform(self, platform):
        # Delete platform
        if input("\n\nAre you 100% certain that you want to delete the {} platform?\nAll deployments and campaigns will also be deleted. \nThis is not reversible. \nIf so, type 'YES'.\n".format(platform['name'])) == "YES":
            #endpoint = self.build_resource_endpoint(self.platform_resource)
            self.delete(resource=self.platform_resource, id=platform['id'])

    def new_platform_data_gui(self, fieldnames):
        fieldvalues = UIComponents.input_multi("Enter platform data", fieldnames)
        return fieldvalues

    def new_platform_gui(self):
        user = self.get_user(token=self.api_token)

        datasource_type = UIComponents.select_list("Choose a DATASOURCE TYPE for the PLATFORM:", ["aws", "http", "local"])
        if datasource_type == "aws":
            platform_data = self.new_platform_data_gui(['name', 'description', 'datafile_pattern', 'media_path_pattern', 'thm_path_pattern', 'datasource_pattern', 'datasource_prefix', 'deployment_name_regex', 'campaign_name_regex'])
        elif datasource_type == "http":
            platform_data = self.new_platform_data_gui(['name', 'description', 'datafile_pattern', 'media_path_pattern', 'thm_path_pattern', 'datasource_pattern', 'deployment_name_regex', 'campaign_name_regex'])
        elif datasource_type == "local":
            platform_data = self.new_platform_data_gui(['name', 'description', 'datafile_pattern', 'media_path_pattern', 'thm_path_pattern', 'datasource_pattern', 'deployment_name_regex', 'campaign_name_regex'])
        else:
            raise ValueError("Unrecognised datasource type: {}".format(datasource_type))

        platform_data['datafile_operations'] = UIComponents.input_json("File operations (as json)")
        platform_data['datasource_type'] = datasource_type

        name = platform_data.pop('name')
        description = platform_data.pop('description')

        return self.new_platform(name, description, user['id'], platform_data)

    def export_deployment(self, id, save_path=None, returns=(), filters=(), transform=None):
        valid_transforms = [None]
        valid_filters = []
        valid_returns = ["pose", "posedata", "mediapaths", "mediapaths-resolved", "datasetkeys", "events"]
        assert transform in valid_transforms, "Invalid 'transform' parameter. Can be one of: {}".format(valid_transforms)
        assert isinstance(filters, (list, tuple)), "Input parameter 'filters' must be a list/tuple."
        assert isinstance(returns, (list, tuple)), "Input parameter 'returns' must be a list/tuple."
        assert all([f in valid_filters for f in filters]), "Invalid 'filters' parameter. Can only be: {}".format(valid_filters)
        assert all([r in valid_returns for r in returns]), "Invalid 'returns' parameter. Can only be: {}".format(valid_returns)

        qsparams = [("format", "csv"), ("file", "attachment")]
        for f in filters:
            qsparams.append(("filter", f))

        for r in returns:
            qsparams.append(("return", r))

        if transform is not None:
            qsparams.append(("transform", transform))

        return self.download_file(resource=self.deployment_resource, id=id, append="/export", save_path=save_path, query_string_params=qsparams)


if __name__ == "__main__":
    print ("Opening GUI to select dataset...")

    logfile = "{}.log".format(os.path.realpath(__file__))
    logging.basicConfig(filename=logfile, level=logging.INFO, format='%(asctime)s %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p')

    args = api_argparser.parse_args()
    url = args.url  # get url from cmd line argument
    api_token = args.api_key  # get api_token from cmd line argument

    sqapi = SQAPIDatasources(api_token=api_token, url=url)

    user = sqapi.get_user(token=api_token)
    platform = sqapi.select_platform()

    campaigndir, campaign_key = sqapi.select_list_gui("Choose a CAMPAIGN:", sqapi.get_datasource_list(platform['data']))
    deploymentdir, deployment_key = sqapi.select_list_gui("Choose a DEPLOYMENT:", sqapi.get_datasource_list(platform['data'], urlkey=campaigndir))

    sqapi.import_deployment(platform, user, campaign_key, deployment_key)

