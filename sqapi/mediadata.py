import json
import time

import numpy as np
try:
    from urllib.request import urlopen
except ImportError:
    from urllib import urlopen
try:
    import cv2
except ImportError:
    print ("WARNING: import cv2 was not found!")


from .users import SQAPIUser, api_argparser


class SQAPIMediaData(SQAPIUser):
    annotation_set_resource = "annotation_set"
    media_resource = "media"
    tag_group_resource = "tag_group"
    tag_scheme_resource = "tag_scheme"
    # tag_scheme_tree_resource = "tag_scheme-tree"
    annotation_resource = "annotation"
    # media_annotations_resource = "media-annotations"
    media_collection_resource = "media_collection"

    def __init__(self, **kwargs):
        SQAPIUser.__init__(self, **kwargs)

    def get_annotation_set(self, annotation_set_id):
        #endpoint = self.build_resource_endpoint(self.annotation_set_resource)
        return self.get(resource=self.annotation_set_resource, id=annotation_set_id)

    def new_annotation_set(self, media_collection_id, annotation_set_name, tag_scheme_id, user_id, parent_annotation_set_id=None, description="", params={}, allow_add=False):
        data = {
            'data': {'allow_add': allow_add, 'params': params},
            'description': description,
            'media_collection_id': media_collection_id,
            'name': annotation_set_name,
            'tag_scheme_id': tag_scheme_id,
            'parent_annotation_set_id': parent_annotation_set_id,
            'user_id': user_id
        }
        #endpoint = self.build_resource_endpoint(self.annotation_set_resource)
        return self.create(data, resource=self.annotation_set_resource)

    def get_media_list(self, media_collection_id, results_per_page=100, page=1):
        #endpoint = self.build_resource_endpoint(self.media_resource)
        filters = [{"name": "media_collections", "op": "any", "val": {"name": "id", "op": "eq", "val": media_collection_id}}]
        order_by = [{"field":"timestamp_start","direction":"asc"}]
        return self.get(resource=self.media_resource, filters=filters, results_per_page=results_per_page, page=page, order_by=order_by)

    def get_tag_scheme_list(self, tag_scheme_id, results_per_page=2000, page=1):
        # "tag_scheme_list": '{api_url}/tag_group?q={{"filters":[{{"name":"tag_scheme_id","op":"eq","val":{tag_scheme_id}}}]}}&results_per_page=5000',
        # Get full list of annotation labels for the selected tag_scheme from API (HTTP GET REQUEST). This may be useful for
        # training a classifier, or for manually reconciling classifier labels with labels contained in the system.
        #endpoint = self.build_resource_endpoint(self.tag_scheme_resource)
        filters = [{"name": "tag_scheme_id", "op": "eq", "val": tag_scheme_id}]
        return self.get(resource=self.tag_group_resource, filters=filters, results_per_page=results_per_page, page=page)

    def get_tag_scheme_hierarchy(self, tag_scheme_id):
        # Below is similar but is a hierarchical representation of the list of classes, with a bit less info
        # "tag_scheme_hierarchy": '{api_url}/tag_scheme-tree/{tag_scheme_id}'
        #endpoint = self.build_resource_endpoint(self.tag_scheme_tree_resource)
        return self.get(resource=self.tag_scheme_resource, id=tag_scheme_id, append="/nested")

    def get_tag_group(self, tag_group_id=None, tag_group_info=None, tag_scheme_id=None, tag_group_name=None):
        #endpoint = self.build_resource_endpoint(self.tag_group_resource)
        if tag_group_id is not None:
            # Get specific class label (tag_group) info using known tag_group_id from API (HTTP GET REQUEST). This may be useful if
            # you use the options above and have specific IDs for classes contained in the API
            #'{api_url}/tag_group/{tag_group_id}'
            return self.get(resource=self.tag_group_resource, id=tag_group_id)
        elif tag_group_info is not None and tag_scheme_id is not None:
            # Get specific class label (tag_group) info using info properties from API (HTTP GET REQUEST)
            #'{api_url}/tag_group?q={{"filters":[{{"name":"info","op":"any","val":{{"name":"name","op":"eq","val":"{key}"}}}},{{"name":"info","op":"any","val":{{"name":"value","op":"eq","val":"{value}"}}}},{{"name":"tag_scheme_id","op":"eq","val":{tag_scheme_id}}}],"single":true}}',
            filters = [
                {"name": "info", "op": "any", "val": {"name": "name", "op": "eq", "val": tag_group_info["key"]}},
                {"name": "info", "op": "any", "val": {"name": "value", "op": "eq", "val": tag_group_info["value"]}},
                {"name": "tag_scheme_id", "op": "eq", "val": tag_scheme_id}
            ]
            return self.get(resource=self.tag_group_resource, filters=filters, single=True)
        elif tag_group_name is not None and tag_scheme_id is not None:
            filters = [{"name": "name", "op": "eq", "val": tag_group_name}, {"name": "tag_scheme_id", "op": "eq", "val": tag_scheme_id}]
            return self.get(resource=self.tag_group_resource, filters=filters, single=True)
        else:
            raise AssertionError("Tag group could not be identified. Not enough valid info.")

    def get_media_annotations(self, media_id, annotation_set_id):
        #"media_annotations": '{api_url}/media/{media_id}/annotations/{annotation_set_id}',
        #endpoint = self.build_resource_endpoint(self.media_annotations_resource)
        return self.get(resource=self.media_resource, id=media_id, append="/annotations/%d" % annotation_set_id, validate_api_output=False)

    def new_annotation(self, annotation):
        #endpoint = self.build_resource_endpoint(self.annotation_resource)
        return self.create(annotation, resource=self.annotation_resource)

    def new_annotation_retry(self, annotation, retries=5):
        retry_count = 0
        result = {"status": "error"}
        while retry_count < retries:
            try:
                retry_count = retry_count+1
                result = self.new_annotation(annotation)
                break
            except Exception as e:
                print ("*** ERROR: Could not create annotation [{}]. RETRYING: {}/{}".format(e, retry_count, retries))
        return result

    def url_to_image(self,url):
        # download the image, convert it to a NumPy array, and then read
        # it into OpenCV format
        print ("Getting image: {}".format(url))
        resp = urlopen(url)
        image = np.asarray(bytearray(resp.read()), dtype="uint8")
        image = cv2.imdecode(image, cv2.IMREAD_COLOR)

        # return the image
        return image

    def export_annotation_set(self, origin_asid, save_path=None, returns=(), filters=(), transform=None, validation_asid=None):
        valid_filters = ["has-label", "has-xy", "doesnthave-xy"]
        valid_transforms = ["validate", "aggregate", "classtally", None]
        valid_returns = ["pose", "posedata", "mediapath", "datasetkeys", "labelinfo", "labelemail", "pointinfo"]
        assert isinstance(filters, list), "Input parameter 'filters' must be a list."
        assert isinstance(returns, list), "Input parameter 'returns' must be a list."
        assert transform in valid_transforms, "Invalid 'transform' parameter. Can be one of: {}".format(valid_transforms)
        assert all([f in valid_filters for f in filters]), "Invalid 'filters' parameter. Can be any of: {}".format(valid_filters)
        assert all([r in valid_returns for r in returns]), "Invalid 'returns' parameter. Can be any of: {}".format(valid_returns)


        qsparams = [("format", "csv"), ("file", "attachment")]
        for f in filters:
            qsparams.append(("filter", f))

        for r in returns:
            qsparams.append(("return", r))

        if validation_asid is not None:
            assert isinstance(validation_asid, int), "Input parameter 'validation_id' must be an integer."
            qsparams.append(("validation_id", validation_asid))
            if transform is None:
                transform = "validate"

        if transform is not None:
            qsparams.append(("transform", transform))

        return self.download_file(resource=self.annotation_set_resource, id=origin_asid, append="/export", save_path=save_path, query_string_params=qsparams)

    def export_media_collection(self, clid, save_path=None, returns=(), filters=()):
        valid_filters = ["has-event"]
        valid_returns = ["pose", "posedata", "mediapath", "datasetkeys", "events"]
        assert isinstance(filters, list), "Input parameter 'filters' must be a list."
        assert isinstance(returns, list), "Input parameter 'returns' must be a list."
        assert all([f in valid_filters for f in filters]), "Invalid 'filters' parameter. Can be any of: {}".format(valid_filters)
        assert all([r in valid_returns for r in returns]), "Invalid 'returns' parameter. Can be any of: {}".format(valid_returns)

        qsparams = [("format", "csv"), ("file", "attachment")]
        for f in filters:
            qsparams.append(("filter", f))

        for r in returns:
            qsparams.append(("return", r))

        return self.download_file(resource=self.media_collection_resource, id=clid, append="/export", save_path=save_path, query_string_params=qsparams)


class SQMediaObject:
    def __init__(self, url, media_type="image"):
        self.url = url
        self.type = media_type
        self.width = None
        self.height = None
        self.duration = None
        self.start_time = None
        self._data = None
        self._processed_data = None
        self.is_processed = False

    def url_to_image(self, url):
        start_time = time.time()
        resp = urlopen(url)
        image = np.asarray(bytearray(resp.read()), dtype="uint8")
        image = cv2.imdecode(image, cv2.IMREAD_COLOR)
        print("Downloaded image: {} in {} s...".format(url, time.time() - start_time))
        return image

    def data(self, processed_data=None):
        if processed_data is not None:
            self._processed_data = processed_data
            self.is_processed = True
        elif self._data is None:
            if self.type == "image":
                self._data = self.url_to_image(self.url)
                data_shape = self._data.shape
                self.height = data_shape[0]
                self.width = data_shape[1]
            else:
                raise ValueError("Unsupported media type: {}".format(self.type))
        return self._data if not self.is_processed else self._processed_data
