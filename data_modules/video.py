import os
import cv2
import argparse
from glob import glob


DEFAULT_JPEG_QUALITY = 90


parser = argparse.ArgumentParser(description="Extract frame from video file")
parser.add_argument('-f', '--filepath', action="store", type=str, help="Full path to Video file")
parser.add_argument('-o', '--save_dir', action="store", type=str, help="Path to output dir")
parser.add_argument('-t', '--runtime', action="store", type=float, help="Runtime to grab frame")


class CVVideo:
    def __init__(self, filepath, save_dir="frames", jpeg_quality=DEFAULT_JPEG_QUALITY):
        self.cvcap = cv2.VideoCapture(filepath)  # load cvtcap for framegrabs
        self.filepath = filepath
        self.jpeg_quality = jpeg_quality

    def capture_frame(self, t, fname):
        self.cvcap.set(cv2.CAP_PROP_POS_MSEC, t * 1000)
        ret, frame = self.cvcap.read()

        # save frame
        if ret:
            if not os.path.isdir(os.path.dirname(fname)):
                os.makedirs(os.path.dirname(fname))
            cv2.imwrite(fname, frame, [int(cv2.IMWRITE_JPEG_QUALITY), self.jpeg_quality])
        else:
            print("Failed to read...")


if __name__ == "__main__":
    args = parser.parse_args()
    vid = CVVideo(args.filepath, save_dir=args.save_dir)
    vid.capture_frame(args.runtime)
