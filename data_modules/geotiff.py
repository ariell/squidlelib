#!/usr/bin/env python

# http://geoinformaticstutorial.blogspot.it/2012/09/reading-raster-data-with-python-and-gdal.html
# http://www.gis.usu.edu/~chrisg/python/2009/lectures/ospy_slides4.pdf
import argparse
# import glob
import pprint

import os
# import re
from osgeo import gdal, ogr, osr
from osgeo.gdalconst import *
import struct

import numpy as np
# import sys
# import LatLon

# import matplotlib
# matplotlib.use('Agg')
# import matplotlib.pyplot as plt


# maptracker_uploads = os.path.join(os.path.dirname(os.path.dirname(os.path.realpath(__file__))), "uploads")
# maptracker_geotiff_path = os.path.join(maptracker_uploads, "geotiffs", "*.raw.tif")
# maptracker_profile_plot = os.path.join(maptracker_uploads, "profile.png")
# maptracker_profile_plot_url = "/uploads/profile.png"

# def pt2fmt(pt):
#     fmttypes = {
#         GDT_Byte: 'B',
#         GDT_Int16: 'h',
#         GDT_UInt16: 'H',
#         GDT_Int32: 'i',
#         GDT_UInt32: 'I',
#         GDT_Float32: 'f',
#         GDT_Float64: 'f'
#     }
#     return fmttypes.get(pt, 'x')


def find_depth_from_multiple_geotiffs(geotiff_list, latlon=None):
    for candidate_geotiff in geotiff_list:
        try:
            depth = latlon_to_depth(candidate_geotiff, latlon)
            print("depth {} m at coords {}, according to geotiff {}".format(depth,latlon,candidate_geotiff))
            return depth
        except:
            print("coords {} not in geottf {}".format(latlon,candidate_geotiff))
    print("no depth found for coords in available geotiffs")
    return None


def latlon_to_depth(filepath, latlon=None):

    ds = gdal.Open(filepath, GA_ReadOnly)
    if ds is None:
        print ('Failed open file')
        raise Exception('Failed open file')

    print("*** Driver: {}/{}".format(ds.GetDriver().ShortName, ds.GetDriver().LongName))
    print("*** Size is {} x {} x {}".format(ds.RasterXSize, ds.RasterYSize, ds.RasterCount))

    transf = ds.GetGeoTransform()
    band = ds.GetRasterBand(1)
    # bandtype = gdal.GetDataTypeName(band.DataType)  # Int16

    print("*** Transform :{}".format(transf))

    # assumed WGS84 (EPSG:4326) lat/lon for input coordinates
    inputEPSG = 4326
    inSpatialRef = osr.SpatialReference()
    inSpatialRef.ImportFromEPSG(inputEPSG)

    # get output SpatialReference from loaded tif
    prj=ds.GetProjection()
    print("Projection {}".format(prj))
    outSpatialRef=osr.SpatialReference(wkt=prj)
    if outSpatialRef.IsProjected:
        print("PROJCS {}".format(outSpatialRef.GetAttrValue('projcs')))
        print("GEOGCS {}".format(outSpatialRef.GetAttrValue('geogcs')))
        # create a geometry from coordinates

    coordTransform = osr.CoordinateTransformation(inSpatialRef,outSpatialRef)




    min = band.GetMinimum()
    max = band.GetMaximum()
    if not min or not max:
        (min,max) = band.ComputeRasterMinMax(True)
    print("*** Band: Min={:.3f}, Max={:.3f}".format(min,max))

    if band.GetOverviewCount() > 0:
        print("*** Band has {} overviews".format(band.GetOverviewCount()))

    if band.GetRasterColorTable():
        print("*** Band has a color table with {} entries".format(band.GetRasterColorTable().GetCount()))


    # success, transfInv = gdal.InvGeoTransform(transf)     # linux?
    transfInv = gdal.InvGeoTransform(transf)                # macos hack...?
    success = transfInv is not None
    if not success:
        print ("Failed InvGeoTransform()")
        raise Exception("Failed InvGeoTransform()")

    #if not isinstance(latlon[0], list):
    #    latlon = [latlon]

    point = ogr.Geometry(ogr.wkbPoint)


    print("latlon {}".format(latlon))
    # print("lon {}".format(latlon[1]))
    # print("lat {}".format(latlon[0]))

    # assuming input is lat lon, this needs x y
    point.AddPoint(latlon[1], latlon[0])
    # transform point
    point.Transform(coordTransform)
    print("transformed point coords  x {}, y {}".format(point.GetX(),point.GetY()))

    # depth = []
    data_array = np.array(band.ReadAsArray())

    px, py = gdal.ApplyGeoTransform(transfInv, point.GetX(), point.GetY())
    print("in coords x {} y {}, pixel coords {} {}".format(point.GetX(),point.GetY(),px,py))
    # depth.append(round(data_array[int(py)][int(px)], 2))
    # return depth
    return round(data_array[int(py)][int(px)], 2)

#
    # for ll in latlon:
    #     px, py = gdal.ApplyGeoTransform(transfInv, ll[1], ll[0])
    #     # old way that was not working for Cardno data
    #     #structval = band.ReadRaster(int(px), int(py), 1, 1, buf_type=band.DataType)
    #     #fmt = pt2fmt(band.DataType)
    #     #intval = struct.unpack(fmt, structval)
    #     depth.append(round(data_array[int(py)][int(px)], 2))
    # return depth

# def interp_latlons(latlon1, latlon2, res=0.005):
#     p1 = LatLon.LatLon(*latlon1)
#     p2 = LatLon.LatLon(*latlon2)
#     distance = p1.distance(p2)
#     heading = p1.heading_initial(p2)
#
#     point_list = []
#     x = []
#     for i in range(0, int((distance-res)/res)):
#         x.append(i*res*1000)
#         p = p1.offset(heading, i*res)
#         point_list.append([float(p.lat), float(p.lon)])
#     point_list.append(latlon2)
#     x.append(distance*1000)
#     # print point_list
#     return point_list, distance, heading, x

#
# def dd2dm(deg):
#     deg = float(deg)
#     d = int(deg)
#     md = round(abs(deg - d) * 60, 4)
#     return "{}&#176; {}'".format(d, md)
#
#
# def dd2dms(deg):
#     d = int(deg)
#     md = abs(deg - d) * 60
#     m = int(md)
#     sd = round((md - m) * 60, 0)
#     return "{}&#176; {}' {}\"".format(d, m, sd)


# def maptracker_latlon_to_depth(lat, lon):
#     global maptracker_geotiff_path
#
#     lat = float(lat)
#     lon = float(lon)
#
#     depth = None
#     files = glob.glob(maptracker_geotiff_path)
#     for f in files:
#         try:
#             depth = latlon_to_depth(f, [lat, lon])
#             break
#         except Exception as e:
#             print ("*** ERROR: {}".format(e))
#
#     return {
#         "depth": depth[0],
#         "latd": round(lat, 8),
#         "lond": round(lon, 8),
#         "latm": dd2dm(lat),
#         "lonm": dd2dm(lon),
#         "latdms": dd2dms(lat),
#         "londms": dd2dms(lon)
#     }
#
#
# def maptracker_depth_profile(lat1, lon1, lat2, lon2, dep1=None, dep2=None, res=5.0):
#     global maptracker_geotiff_path
#     latlon_list, distance, heading, x = interp_latlons([float(lat1), float(lon1)], [float(lat2), float(lon2)], res=res/1000)
#
#     depth = []
#     files = glob.glob(maptracker_geotiff_path)
#     for f in files:
#         try:
#             depth = latlon_to_depth(f, latlon_list)
#             break
#         except Exception as e:
#             print ("*** ERROR: {}".format(e))
#
#     # x = [res*i for i in range(0, len(depth))]
#     slope = np.zeros(np.array(depth).shape, np.float)
#     slope[0:-1] = np.diff(depth) / np.diff(x)
#     slope[-1] = (depth[-1] - depth[-2]) / (x[-1] - x[-2])
#
#
#     fig = two_scales(x, depth, slope, dep1, dep2)
#
#
#
#
#     # plt.plot([res*i for i in range(0, len(depth))], depth)
#     # fig = plt.gcf()
#     fig.set_size_inches(10, 6)
#     fig.savefig(maptracker_profile_plot)
#
#     return {
#         "plot_url": maptracker_profile_plot_url,
#         "distance": distance*1000,
#         "heading": heading,
#         "res": res
#     }
#
#
# def two_scales(x, data1, data2, dep1, dep2):
#     fig, ax1 = plt.subplots()
#
#     ax1.plot(x, data1, color="k")
#     if dep1 is not None and dep2 is not None:
#         ax1.plot([x[0], x[-1]], [float(dep1), float(dep2)], color="r", linestyle=":")
#
#     ax1.set_xlabel('Distance (m)')
#     ax1.set_ylabel('Depth (m)')
#
#     ax2 = ax1.twinx()
#     ax2.plot(x, data2, color="k", alpha=0.2)
#     ax2.set_ylabel('Slope')
#
#     color_y_axis(ax1, "k")
#     color_y_axis(ax2, "0.8")
#     return fig


# Change color of each axis
def color_y_axis(ax, color):
    """Color your axes."""
    for t in ax.get_yticklabels():
        t.set_color(color)
    return None

def main():
    # Parse input args
    parser = argparse.ArgumentParser(description="Get depth from lat lon in GeoTiff")
    parser.add_argument('filepath', action="store", type=str, help="Full path to GeoTiff file")
    parser.add_argument('lat', action="store", type=float, help="Latitude in decimal degrees")
    parser.add_argument('lon', action="store", type=float, help="Longitude in decimal degrees")

    # Set options
    args = parser.parse_args()
    pprint.pprint (latlon_to_depth(args.filepath, [args.lat, args.lon]))

if __name__ == "__main__":
    main()
